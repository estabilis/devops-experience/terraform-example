resource "aws_vpc" "experience" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "experience"
  }
}

resource "aws_subnet" "experience" {
  vpc_id     = aws_vpc.experience.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"

  tags = {
    Name = "public-experience"
  }
}

resource "aws_internet_gateway" "experience" {
  vpc_id = aws_vpc.experience.id

  tags = {
    Name = "ig-experience"
  }
}

resource "aws_route_table_association" "experience" {
  subnet_id      = aws_subnet.experience.id
  route_table_id = aws_route_table.experience.id
}

resource "aws_route_table" "experience" {
  vpc_id = aws_vpc.experience.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.experience.id
  }

  tags = {
    Name = "rt-experience"
  }
}

resource "aws_network_interface" "experience" {
  subnet_id   = aws_subnet.experience.id

  tags = {
    Name = "eni-experience"
  }
}

resource "aws_instance" "experience" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t3.micro"
  key_name      = "experience-key"


  network_interface {
    network_interface_id = aws_network_interface.experience.id
    device_index         = 0
  }

  tags = {
    Name = "ec2-experience"
  }
}