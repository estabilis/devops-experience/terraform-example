terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "experience-tf-backend"
    key    = "state/terraform.state"
    region = "us-east-1"
  }
}

provider "aws" {}

